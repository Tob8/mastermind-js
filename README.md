# MASTERMIND

## How the game works

- You win if your colors match the random color choice.
- Click 4 colors, correct it with ⏪ and check it with ✅.
- You get a ⚪️ hint for every correct color at the wrong position
- You get a ⚫️ hint for every correct color at the correct position.
- The order of the hints does not match with the positions.
- You have max 12 round to find the correct match - have fun 🥳

# Preview
<div style="display: flex"}>
  <img src="https://tobiasgujer.com/gitlab/preview-mastermind-start.png" alt="Preview of of game start" width="500" height="100%"/>
  <img src="https://tobiasgujer.com/gitlab/preview-mastermind-full-game.png" alt="Preview of full game" width="500" heigth="100%"/>
</div>

# Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.