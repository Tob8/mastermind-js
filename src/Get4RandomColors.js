const Get4RandomColors = (col, num) => {
  const shuffled = [...col].sort(() => 0.5 - Math.random())
  return shuffled.slice(0, num)
}

export default Get4RandomColors