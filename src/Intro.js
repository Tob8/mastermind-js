/* eslint-disable jsx-a11y/accessible-emoji */
import React from 'react'
import './App.css'
import ButtonNewGame from './ButtonNewGame';

const Game = () => {
  return (
    <div className="intro">
      <top>
        <h1>MASTERMIND</h1>
      </top>
      <middle>
        <h3>How the game works:</h3>
        <li>You win if your colors match the random color choice.</li>
        <li>Click 4 colors, correct it with ⏪ and check it with ✅.</li>
        <li>You get a ⚪️ hint for every correct color at the wrong position</li>
        <li>You get a ⚫️ hint for every correct color at the correct position.</li>
        <li>The order of the hints does not match with the positions.</li>
        <li>You have max 10 round to find the correct match - have fun 🥳</li>
      </middle>
      <low>
        {/* TODO: functionality of button */}
        <ButtonNewGame />
      </low>
    </div>
  )
}

export default Game